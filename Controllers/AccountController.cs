﻿using System;
using M8_UF3_Examen.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace M8_UF3_Examen.Controllers
{
    public class AccountController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Login(UserCredentials credentials)
        {
            if (IsValidUser(credentials))
            {
                HttpContext.Session.SetString("User",credentials.User);
                return RedirectToAction("Index", "Home");
            }
            return View(credentials);
        }

        private bool IsValidUser(UserCredentials credentials)
        {
            //TODO
            return false;
        }
    }
}