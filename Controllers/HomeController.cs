﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using M8_UF3_Examen.Models;
using Microsoft.AspNetCore.Http;

namespace M8_UF3_Examen.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("User") == null) return RedirectToAction("Login", "Account");
            ViewData["User"] = HttpContext.Session.GetString("User");
            return View();
        }

        public IActionResult Users()
        {
            if (HttpContext.Session.GetString("User") == null) return RedirectToAction("Login", "Account");
            ViewData["User"] = HttpContext.Session.GetString("User");

            List<LdapUser> users = GetUsers();
            
            return View(users);
        }

        private List<LdapUser> GetUsers()
        {
            //TODO
            return null;
        }
    }
}